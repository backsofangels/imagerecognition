//
//  CoreMLManager.swift
//  ImageRecognition
//
//  Created by Salvatore Penitente on 13/06/2017.
//  Copyright © 2017 Salvatore Penitente. All rights reserved.
//

import Foundation
import Vision
import CoreML
import UIKit

protocol CoreMLManagerDelegate {
    func manageResult(_ resultToProcess: VNClassificationObservation)
    func errorManagement(_ error: Error)
}

class CoreMLManager {
    
    var coreMLModel: VNCoreMLModel!
    var imageToAnalyze: CGImage!
    
    var delegate: CoreMLManagerDelegate?
    
    init (VNcoreMLModelInstance: VNCoreMLModel, imageToScan: UIImage) {
        self.coreMLModel = VNcoreMLModelInstance
        self.imageToAnalyze = imageToScan.cgImage
    }
    
    func performNeuralNetworkRequest() {
        let neuralRequest = VNCoreMLRequest(model: coreMLModel, completionHandler: requestHandler)
        let imageRequestHandler = VNImageRequestHandler(cgImage: self.imageToAnalyze, options: [:])
        do {
            try imageRequestHandler.perform([neuralRequest])
        }
        catch let error {
            self.delegate?.errorManagement(error)
        }
    }
    
    private func requestHandler(_ request: VNRequest, error: Error?) {
        guard let results = request.results as? [VNClassificationObservation] else {
            fatalError("Error in the request")
        }
        for classification in results {
            self.delegate?.manageResult(classification)
        }
    }
}
