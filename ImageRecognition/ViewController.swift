//
//  ViewController.swift
//  ImageRecognition
//
//  Created by Salvatore Penitente on 12/06/2017.
//  Copyright © 2017 Salvatore Penitente. All rights reserved.
//

import UIKit
import Vision
import AVFoundation
import Photos

class ViewController: UIViewController, CoreMLManagerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var resultLabel: UILabel!
    
    @IBAction func photoCaptureButton(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            print("Button capture")
            pickerController = UIImagePickerController()
            pickerController.delegate = self
            pickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
            pickerController.allowsEditing = false
            let alertController = UIAlertController(title: "Take photo", message: "Photo or camera?", preferredStyle: .actionSheet)
            self.present(pickerController, animated: false, completion: nil)
        }
    }
    
    @IBAction func scanButton(_ sender: Any) {
        neuralManager.performNeuralNetworkRequest()
    }
    
    var model: VNCoreMLModel!
    var neuralManager: CoreMLManager!
    var imageToScan: UIImage!
    var pickerController: UIImagePickerController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.image = imageToScan
        do {
            model = try VNCoreMLModel(for: Inceptionv3().model)
        }
        catch { print("error") }
        
        neuralManager = CoreMLManager(VNcoreMLModelInstance: model, imageToScan: imageToScan!)
        neuralManager.delegate = self
    }
    
    func manageResult(_ resultToProcess: VNClassificationObservation) {
        var resultString = String()
        resultString = resultString + resultToProcess.identifier
        resultLabel.text = resultString
    }
    
    func errorManagement(_ error: Error) {
        print("ops, we got an error \(error)")
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        imageToScan = selectedImage
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

